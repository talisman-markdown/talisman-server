#!/usr/bin/zsh

go install
mkdir -p $HOME/.config/coyote
rsync -a ./config/* $HOME/.config/coyote
fuser -k 2766/tcp
echo "Rebuild successful on $(date +%H:%M)"