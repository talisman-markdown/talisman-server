#!/usr/bin/env zsh

if [[ ! -d node_modules ]]; then
   npm install
fi  
go install
mkdir -p $HOME/.config/talisman-server
rsync -a ./config/* $HOME/.config/talisman-server
node node_modules/minify/bin/minify.js config/static/styles.css > $HOME/.config/talisman-server/static/styles.css
echo "Installation is complete"
