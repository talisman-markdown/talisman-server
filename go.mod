module talisman-server

go 1.15

require (
	github.com/gorilla/mux v1.8.0
	github.com/joho/godotenv v1.3.0
	github.com/mdigger/goldmark-attributes v0.0.0-20191228154645-1cb795f70464
	github.com/yuin/goldmark v1.2.1
	github.com/yuin/goldmark-meta v1.0.0
)
