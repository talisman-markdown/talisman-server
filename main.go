package main

import (
	"flag"
	"fmt"
	"html/template"
	"io"
	"log"
	"net/http"
	"os"
	"os/user"
	"path/filepath"

	"github.com/gorilla/mux"
	"github.com/joho/godotenv"
)

var (
	usr, _  = user.Current()
	confdir = filepath.Join(usr.HomeDir, ".config/talisman-server/")
	tplglob = filepath.Join(confdir, "templates/*.html")
	tpls    = template.Must(template.ParseGlob(tplglob))
	static  = filepath.Join(confdir, "static")
)

//Page denotes a single wiki page and its metadata
type Page struct {
	Slug string
	Body template.HTML
}

func main() {
	// Change working directory to one supplied via flag. Use as talisman-server -d "path todirectory under home folder"
	d := flag.String("d", "zettel", "directory to serve")
	flag.Parse()
	cwd := filepath.Join(usr.HomeDir, *d)
	err := os.Chdir(cwd)
	if err != nil {
		panic(err)
	}

	// Load environment variables including port from .env file in the directory if it exits
	err = godotenv.Load()
	if err != nil {
		log.Println("Error loading .env file")
	}
	port := os.Getenv("PORT")
	if port == "" {
		port = "2766"
	}

	// Routing
	r := mux.NewRouter()
	r.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir(static))))
	r.PathPrefix("/images/").Handler(http.StripPrefix("/images/", http.FileServer(http.Dir("images")))) //TODO: Take this value from config
	r.HandleFunc("/favicon.ico", faviconHandler)
	r.HandleFunc("/{title}", viewHandler)
	r.Handle("/", injectCSS(http.FileServer(http.Dir(cwd))))

	fmt.Printf("Server starting at http://127.0.0.1:%s\n", port)
	log.Fatal(http.ListenAndServe(":"+port, r)) //TODO: Snippetize how to add colon to port
}

// Pass Stylesheets to Directory listing
func injectCSS(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		h.ServeHTTP(w, r)
		io.WriteString(w, `<link rel="stylesheet" href="/static/fonts.css"><link rel="stylesheet" href="/static/index.css">`)
	})

}

func viewHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	title := vars["title"]
	log.Println("Loading ", title)
	p, err := loadPage(title)
	if err != nil {
		fmt.Printf("Error while loading %s", title)
	}
	renderTemplate(w, "view", p)
}

func faviconHandler(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, filepath.Join(static, "favicon.ico"))
}

func renderTemplate(w http.ResponseWriter, tmpl string, p *Page) {
	err := tpls.ExecuteTemplate(w, tmpl+".html", p)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}
