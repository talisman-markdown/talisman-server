package main

import (
	"bytes"
	"html/template"
	"io/ioutil"
	"log"
	"strings"

	"github.com/mdigger/goldmark-attributes"
	"github.com/yuin/goldmark"
	"github.com/yuin/goldmark/extension"
	"github.com/yuin/goldmark/parser"
	"github.com/yuin/goldmark/renderer/html"
)

func loadPage(title string) (*Page, error) {
	if !strings.HasSuffix(title, ".md") {
		title = title + ".md"
	}
	body, err := ioutil.ReadFile(title)
	if err != nil {
		log.Println(err)
	}
	markdown := goldmark.New(
		goldmark.WithExtensions(
			extension.Table,
			extension.Linkify,
			extension.TaskList,
			extension.DefinitionList,
			extension.Strikethrough,
		),
		goldmark.WithParserOptions(
			parser.WithAutoHeadingID(),
		),
		goldmark.WithRendererOptions(
			html.WithHardWraps(),
			html.WithUnsafe(),
			html.WithXHTML(),
		),
		attributes.Enable,
	)
	var buf bytes.Buffer
	context := parser.NewContext()
	if err := markdown.Convert(body, &buf, parser.WithContext(context)); err != nil {
		panic(err)
	}

	bodyhtml := template.HTML(buf.String())

	return &Page{Slug: title, Body: bodyhtml}, nil
}
